﻿using Flatrow.Utils;
using NLog;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Yota.Classes
{
    public class YotaAccount
    {
        protected Logger log = LogManager.GetCurrentClassLogger();

        protected static class Constant
        {
            public const string LoginUri = "https://my.yota.ru/selfcare/login?goto=https%3A%2F%2Fmy.yota.ru%3A443%2Fdevices";
            public const string LoginFormUri = "https://login.yota.ru/UI/Login";
            public const string DevicesUri = "https://my.yota.ru/selfcare/devices";
            public const string ChangeOfferUri = "https://my.yota.ru/selfcare/devices/changeOffer";
            public const string LoginData = "goto=https%3A%2F%2Fmy.yota.ru%3A443%2Fselfcare%2FloginSuccess&gotoOnFail=https%3A%2F%2Fmy.yota.ru%3A443%2Fselfcare%2FloginError&org=customer";
            public const string LoginFormat = "&old-token={0}&IDToken2={1}&IDToken1={2}";
            public const string LoginUidUri = "https://my.yota.ru/selfcare/login/{0}?value={1}";
            public const string ChangeOfferFormat = "product={0}&offerCode={1}&homeOfferCode=&areOffersAvailable=false&period={2} {3}&status=custom&autoprolong=1&isSlot=false" +
                "&resourceId={4}&currentDevice=1&username=&isDisablingAutoprolong=false";
            public const string ContinueFreeUri = "http://hello.yota.ru/light/";
            public const string ContinueFreeUriPost = "http://hello.yota.ru/php/go.php";
            public const string ContinueFreeUriFormat = "accept_lte=1&redirurl=http://www.yota.ru/&city=C&connection_type=light&service_id=Sliders_Free_Temp";
            public const string ContinueFreeUriReferer = "http://hello.yota.ru/light/";
        }

        protected CookieContainer cookies;

        protected string username, password;

        /// <summary>
        /// Respresents connector to Yota Account
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        public YotaAccount(string username, string password)
        {
            this.username = username;
            this.password = password;
        }

        /// <summary>
        /// Connect to Yota account
        /// </summary>
        public async Task Connect()
        {
            cookies = new CookieContainer();
            HttpWebRequest request = new PureWebRequest(Constant.LoginUri, HttpWebRequestMethod.Get, cookies);
            using (HttpWebResponse response = await request.PureGetResponseAsync())
            {
                FillCookies(response.Cookies);
                await Login(username, password, await GetYotaUid(username));
            }
        }

        /// <summary>
        /// Logging in account
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <param name="yotaUid">Login Uid</param>
        protected async Task Login(string username, string password, string yotaUid)
        {
            HttpWebRequest request = new PureWebRequest(Constant.LoginFormUri, HttpWebRequestMethod.Post, cookies, Constant.LoginUri);
            request.SetRequestData(Constant.LoginData + String.Format(Constant.LoginFormat, username, password, yotaUid));
            using (HttpWebResponse response = await request.PureGetResponseAsync())
            {
                FillCookies(response.Cookies);
            }
        }

        /// <summary>
        /// Gets identifier of Yota's user
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>YotaUID</returns>
        protected async Task<string> GetYotaUid(string username)
        {
            string type = username.Contains('@') ? "getUidByMail" : "getUidByPhone";
            HttpWebRequest request = new PureWebRequest(String.Format(Constant.LoginUidUri, type, username), HttpWebRequestMethod.Get, cookies, Constant.LoginUri);
            using (HttpWebResponse response = await request.PureGetResponseAsync())
            {
                FillCookies(response.Cookies);
                return response.GetResponseString().Replace("ok|", "");
            }
        }

        /// <summary>
        /// Gets page with tariffs
        /// </summary>
        /// <returns>Raw page with tariffs</returns>
        public async Task<string> GetTariffPage()
        {
            HttpWebRequest request = new PureWebRequest(Constant.DevicesUri, HttpWebRequestMethod.Get, cookies);
            using (HttpWebResponse response = await request.PureGetResponseAsync())
            {
                var html = response.GetResponseString();
                if (html.Length < 10)
                {
                    await Connect();
                    if (html.Length < 10)
                    {
                        throw new UnauthorizedAccessException("Failed to log in account");
                    }
                }
                return html;
            }
        }

        /// <summary>
        /// Set new tariff
        /// </summary>
        /// <param name="tariff">Tariff instance</param>
        /// <param name="product">Device param</param>
        /// <param name="resource">Device param</param>
        /// <returns></returns>
        public async Task SetTariff(Tariff tariff, string product, string resource)
        {
            HttpWebRequest request = new PureWebRequest(Constant.ChangeOfferUri, HttpWebRequestMethod.Post, cookies, Constant.DevicesUri);
            request.SetRequestData(String.Format(Constant.ChangeOfferFormat, product, tariff.Code, tariff.Time, tariff.InfoTime, resource));
            await request.PureGetResponseAsync();
        }

        public async Task ContinueFree()
        {
            try
            {
                HttpWebRequest request = new PureWebRequest(Constant.ContinueFreeUri, HttpWebRequestMethod.Get, cookies);
                await request.PureGetResponseAsync();
                request = new PureWebRequest(Constant.ContinueFreeUriPost, HttpWebRequestMethod.Post, cookies,
                    Constant.ContinueFreeUri);
                request.SetRequestData(Constant.ContinueFreeUriFormat);
                await request.PureGetResponseAsync();
            }
            catch (Exception)
            {
                log.Debug("Unable to continue free use");
            }
        }

        /// <summary>
        /// Add cookies collection to local storage
        /// </summary>
        /// <param name="cookies">Cookie collection for adding</param>
        protected void FillCookies(CookieCollection cookies)
        {
            foreach (Cookie cookie in cookies)
                this.cookies.Add(cookie);
        }
    }
}