﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Flatrow.Utils;

namespace Yota.Classes
{
    public class YotaDevice
    {
        protected SelectedList<Tariff> tariffList;

        /// <summary>
        /// Working with account
        /// </summary>
#if DEBUG
        public YotaAccount account;
#else
        protected YotaAccount account;
#endif
        /// <summary>
        /// Device options
        /// </summary>
        protected string product, resource;
        /// <summary>
        /// List of tariffs
        /// </summary>
        public SelectedList<Tariff> TariffList
        {
            get
            {
                return tariffList;
            }
        }

        /// <summary>
        /// Represents for Yota device management
        /// </summary>
        /// <param name="yotaAccount"></param>
        public YotaDevice(YotaAccount yotaAccount)
        {
            account = yotaAccount;
        }

        /// <summary>
        /// Initialize device using account connection
        /// </summary>
        /// <returns>Device instance</returns>
        public async Task Connect()
        {
            tariffList = await GetTariffBase();
        }

        /// <summary>
        /// Loads list of tariffs
        /// </summary>
        /// <returns>List of tariffs</returns>
        protected async Task<SelectedList<Tariff>> GetTariffBase()
        {
            var result = ParseTariffList(await account.GetTariffPage());
            if (result.All(x => x.Time.Equals("30")))
            {
                await account.ContinueFree();
            }
            return result;
        }

        /// <summary>
        /// Set new active tariff
        /// </summary>
        /// <param name="index">Index of the new tariff</param>
        /// <returns></returns>
        public async Task SetTariff(byte index)
        {
            if (index != tariffList.Index)
            {
                await Operation.ExecuteWithRetryAsync(async () =>
                {
                    await account.SetTariff(tariffList[index], product, resource);
                    tariffList = await GetTariffBase();
                }, new TimeSpan(1000), 2);
            }
        }

        /// <summary>
        /// Parses HTML into TariffList
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        protected SelectedList<Tariff> ParseTariffList(string html)
        {
            var result = new SelectedList<Tariff>();
            var data = new Dictionary<string, string>();

            var regexData = new Regex("(\\{\"position\":\"(?<pos>.*?)\".*?\"code\":\"(?<code>.*?)\".*?\\})", RegexOptions.Multiline | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            var regexPairs = new Regex(@""".*?"":.*?,");
            var regexActive = new Regex(@"name=""offerCode"" value=""(?<code>.*?)""");
            var regexProduct = new Regex("<input type=\"hidden\" name=\"product\" value=\"(?<product>.*?)\" />");
            var regexResource = new Regex("<input type=\"hidden\" name=\"resourceId\" value=\"(?<resource>.*?)\" />");

            html = html.Replace("<div class=\\\"max-value\\\">Макс.<\\/div>", "Макс.");
            var activeCode = regexActive.Match(html).Groups["code"].Value;
            product = regexProduct.Match(html).Groups["product"].Value;
            resource = regexResource.Match(html).Groups["resource"].Value;

            foreach (Match match in regexData.Matches(html))
            {
                data.Clear();
                var posData = match.Value.Replace("{", "").Replace("}", "") + ",";
                foreach (Match kvp in regexPairs.Matches(posData))
                {
                    var pair = kvp.Value.Split(':');
                    data.Add(Regex.Replace(pair[0], @"[^\w]", "", RegexOptions.Compiled), pair[1].Replace("\"", "").Replace(",", ""));
                }
                if (!data["position"].Equals("null"))
                {
                    if (!data.ContainsKey("remainNumber"))
                    {
                        continue;
                    }
                    if (data["remainNumber"].Last().Equals('1'))
                    {
                        data["remainString"] += " остался";
                    }
                    else
                    {
                        data["remainString"] += " осталось";
                    }
                    result.Add(new Tariff(data["amountNumber"], data["speedNumber"], data["remainNumber"], data["amountString"], data["speedString"], data["remainString"], data["code"]));
                    if (activeCode.Equals(data["code"]))
                    {
                        result.Index = Convert.ToByte(result.Count - 1);
                    }
                }
            }

            return result;
        }
    }
}