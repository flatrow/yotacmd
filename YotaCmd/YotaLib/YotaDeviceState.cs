﻿using Flatrow.Utils;
using NLog;
using System;
using System.ComponentModel;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Timers;

namespace Yota.Classes
{
    public class YotaDeviceState : INotifyPropertyChanged
    {
        protected Logger log = LogManager.GetCurrentClassLogger();

        protected const int TimerInterval = 1250;

        protected const string YotaStatusUri = "http://status.yota.ru/status";
        protected const string RegexCurrentDownload = @"CurDownlinkThroughput=(?<value>\d+)?";
        protected const string RegexCurrentUpload = @"CurUplinkThroughput=(?<value>\d+)?";
        protected const string RegexMaximumDownload = @"MaxDownlinkThroughput=(?<value>\d+)?";
        protected const string RegexMaximumUpload = @"MaxUplinkThroughput=(?<value>\d+)?";
        protected const string RegexBytesDownload = @"ReceivedBytes=(?<value>\d+)?";
        protected const string RegexBytesUpload = @"SentBytes=(?<value>\d+)?";
        protected const string RegexSignalSinr = @"3GPP.SINR=(?<value>(-)?\d+)?";
        protected const string RegexSignalRsrp = @"3GPP.RSRP=(?<value>(-)?\d+)?";
        protected const string RegexIpAddress = @"IP=(?<value>.*)?";

        protected static string MatchRegex(string value, string regex)
        {
            return new Regex(regex).Match(value).Result("${value}");
        }

        protected double currentDownload, currentUpload;
        protected double maximumDownload, maximumUpload;
        protected double bytesDownload, bytesUpload;
        protected string currentDownloadLabel, currentUploadLabel;
        protected string maximumDownloadLabel, maximumUploadLabel;
        protected string bytesDownloadLabel, bytesUploadLabel;
        protected string signalSinr, signalRsrp;
        protected string ipAddress;
        protected long networkBytesSent, networkBytesReceived;
        protected int networkMaximumDownload, networkMaximumUpload;

        public double CurrentDownload
        {
            get { return currentDownload; }
            set
            {
                currentDownload = value.FromBytesToValue();
                currentDownloadLabel = value.FromBytesToTitle();
                OnPropertyChanged();
                OnPropertyChanged("CurrentDownloadLabel");
            }
        }

        public string CurrentDownloadLabel
        {
            get { return currentDownloadLabel; }
        }

        public double CurrentUpload
        {
            get { return currentUpload; }
            set
            {
                currentUpload = value.FromBytesToValue();
                currentUploadLabel = value.FromBytesToTitle();
                OnPropertyChanged();
                OnPropertyChanged("CurrentUploadLabel");
            }
        }

        public string CurrentUploadLabel
        {
            get { return currentUploadLabel; }
        }

        public double MaximumDownload
        {
            get { return maximumDownload; }
            set
            {
                maximumDownload = value.FromBytesToValue();
                maximumDownloadLabel = value.FromBytesToTitle();
                OnPropertyChanged();
                OnPropertyChanged("MaximumDownloadLabel");
            }
        }

        public string MaximumDownloadLabel
        {
            get { return maximumDownloadLabel; }
        }

        public double MaximumUpload
        {
            get { return maximumUpload; }
            set
            {
                maximumUpload = value.FromBytesToValue();
                maximumUploadLabel = value.FromBytesToTitle();
                OnPropertyChanged();
                OnPropertyChanged("MaximumUploadLabel");
            }
        }

        public string MaximumUploadLabel
        {
            get { return maximumUploadLabel; }
        }

        public double BytesDownload
        {
            get { return bytesDownload; }
            set
            {
                bytesDownload = value.FromBytesToValue();
                bytesDownloadLabel = value.FromBytesToTitle();
                OnPropertyChanged();
                OnPropertyChanged("BytesDownloadLabel");
            }
        }

        public string BytesDownloadLabel
        {
            get { return bytesDownloadLabel; }
        }

        public double BytesUpload
        {
            get { return bytesUpload; }
            set
            {
                bytesUpload = value.FromBytesToValue();
                bytesUploadLabel = value.FromBytesToTitle();
                OnPropertyChanged();
                OnPropertyChanged("BytesUploadLabel");
            }
        }

        public string BytesUploadLabel
        {
            get { return bytesUploadLabel; }
        }

        public string SignalSinr
        {
            get { return signalSinr; }
            set
            {
                signalSinr = value;
                OnPropertyChanged();
            }
        }

        public string SignalRsrp
        {
            get { return signalRsrp; }
            set
            {
                signalRsrp = value;
                OnPropertyChanged();
            }
        }

        public string IpAddress
        {
            get { return ipAddress; }
            set
            {
                ipAddress = value;
                OnPropertyChanged();
            }
        }

        public YotaDeviceState()
        {
            var updateTimer = new Timer(TimerInterval);
            updateTimer.Elapsed += (sender, args) => Update();
            updateTimer.Start();
        }

        protected async void Update()
        {
            var networkInterface = NetworkInterface.GetAllNetworkInterfaces()[NetworkInterface.LoopbackInterfaceIndex];
            var interfaceStatistic = networkInterface.GetIPv4Statistics();

            var bytesReceivedSpeed = (int)(interfaceStatistic.BytesReceived - networkBytesReceived) * 8 / TimerInterval * 1000;
            var bytesSentSpeed = (int)(interfaceStatistic.BytesSent - networkBytesSent) * 8 / TimerInterval * 1000;
            networkMaximumDownload = networkMaximumDownload > bytesReceivedSpeed || networkBytesReceived == 0 ? networkMaximumDownload : bytesReceivedSpeed;
            networkMaximumUpload = networkMaximumUpload > bytesSentSpeed || networkBytesReceived == 0 ? networkMaximumUpload : bytesSentSpeed;
            networkBytesReceived = interfaceStatistic.BytesReceived;
            networkBytesSent = interfaceStatistic.BytesSent;

            try
            {
                HttpWebRequest request = new PureWebRequest(YotaStatusUri, HttpWebRequestMethod.Get);
                using (var response = await request.PureGetResponseAsync())
                {
                    var text = response.GetResponseString();
                    CurrentDownload = Math.Max(Convert.ToInt64(MatchRegex(text, RegexCurrentDownload)), bytesReceivedSpeed);
                    CurrentUpload = Math.Max(Convert.ToInt64(MatchRegex(text, RegexCurrentUpload)), bytesSentSpeed);
                    MaximumDownload = Math.Max(Convert.ToInt64(MatchRegex(text, RegexMaximumDownload)), networkMaximumDownload);
                    MaximumUpload = Math.Max(Convert.ToInt64(MatchRegex(text, RegexMaximumUpload)), networkMaximumUpload);
                    BytesDownload = Convert.ToInt64(MatchRegex(text, RegexBytesDownload));
                    BytesUpload = Convert.ToInt64(MatchRegex(text, RegexBytesUpload));
                    SignalSinr = MatchRegex(text, RegexSignalSinr);
                    SignalRsrp = MatchRegex(text, RegexSignalRsrp);
                    IpAddress = MatchRegex(text, RegexIpAddress);
                }
            }
            catch (Exception ex)
            {
                CurrentDownload = bytesReceivedSpeed;
                CurrentUpload = bytesSentSpeed;
                MaximumDownload = networkMaximumDownload;
                MaximumUpload = networkMaximumUpload;
                BytesDownload = networkBytesReceived * 8;
                BytesUpload = networkBytesSent * 8;
                SignalSinr = "0";
                SignalRsrp = "0";
                IpAddress = "Unknown";
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}