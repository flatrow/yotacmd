﻿using System;
using System.Collections.Generic;

namespace Yota.Classes {
    /// <summary>
    /// Represents list with index of selected one
    /// </summary>
    public class SelectedList<T> : List<T> {

        protected byte index;

        /// <summary>
        /// Index of the selected item
        /// </summary>
        public byte Index
        {
            get
            {
                return index;
            }
            set
            {
                if (value < 0 || value > this.Count - 1)
                {
                    throw new IndexOutOfRangeException();
                }
                index = value;
            }
        }
    }

    /// <summary>
    /// Respersents Yota tariff
    /// </summary>
    public class Tariff {

        protected string price, speed, time, infoPrice, infoSpeed, infoTime, code;

        public string Price {
            get
            {
                return price;
            }
        }
        public string Speed {
            get
            {
                return speed;
            }
        }
        public string Time {
            get
            {
                return time;
            }
        }
        public string InfoPrice {
            get
            {
                return infoPrice;
            }
        }
        public string InfoSpeed {
            get
            {
                return infoSpeed;
            }
        }
        public string InfoTime {
            get
            {
                return infoTime;
            }
        }
        public string Code {
            get
            {
                return code;
            }
        }

        public Tariff(string price, string speed, string time, string infoPrice, string infoSpeed, string infoTime, string code) {
            this.price = price;
            this.speed = speed;
            this.time = time;
            this.infoPrice = infoPrice;
            this.infoSpeed = infoSpeed;
            this.infoTime = infoTime;
            this.code = code;
        }
    }
}
