﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Installer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string AppName = "YotaCmd";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (Environment.Is64BitOperatingSystem)
                textBoxPath.Text = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\" + AppName;
            else
                textBoxPath.Text = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\" + AppName;
        }
    }
}
