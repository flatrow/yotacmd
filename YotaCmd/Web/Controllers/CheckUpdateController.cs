﻿using Flatrow.Utils;

namespace Web.Controllers
{
    public class CheckUpdateController : BaseApiController
    {
        public CheckUpdateController()
            : base(1)
        {
        }

        public BaseResponse Get(string version)
        {
            return WithVersion(() => new BaseResponse<string>("NO"));
        }
    }
}