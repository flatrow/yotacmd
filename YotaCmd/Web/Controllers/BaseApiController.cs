﻿using Flatrow.Utils;
using System;
using System.Web.Http;

namespace Web.Controllers
{
    public class BaseApiController : ApiController
    {
        protected Func<Func<BaseResponse>, BaseResponse> WithVersion;

        public BaseApiController(int version)
        {
            WithVersion = x =>
            {
                var result = x();
                result.Version = version;
                return result;
            };
        }
    }
}