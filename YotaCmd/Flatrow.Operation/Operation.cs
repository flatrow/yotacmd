﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Flatrow.Operation
{
    public class Operation
    {
        public static void ExecuteWithRetry(Action action, TimeSpan retryInterval, int retryCount = 3)
        {
            var exception = new Exception();
            for (int i = 0; i < retryCount; i++)
            {
                try
                {
                    action();
                    return;
                }
                catch (Exception ex)
                {
                    exception = ex;
                    Thread.Sleep(retryInterval);
                }
            }
            throw exception;
        }

        public static T ExecuteWithRetry<T>(Func<T> action, TimeSpan retryInterval, int retryCount = 3)
        {
            var exception = new Exception();
            for (int i = 0; i < retryCount; i++)
            {
                try
                {
                    return action();
                }
                catch (Exception ex)
                {
                    exception = ex;
                    Thread.Sleep(retryInterval);
                }
            }
            throw exception;
        }

        public static async Task ExecuteWithRetryAsync(Func<Task> action, TimeSpan retryInterval, int retryCount = 3)
        {
            var exception = new Exception();
            for (int i = 0; i < retryCount; i++)
            {
                try
                {
                    await action();
                    return;
                }
                catch (Exception ex)
                {
                    exception = ex;
                    Thread.Sleep(retryInterval);
                }
            }
            throw exception;
        }

        public static async Task<T> ExecuteWithRetryAsync<T>(Func<Task<T>> action, TimeSpan retryInterval, int retryCount = 3)
        {
            var exception = new Exception();
            for (int i = 0; i < retryCount; i++)
            {
                try
                {
                    return await action();
                }
                catch (Exception ex)
                {
                    exception = ex;
                    Thread.Sleep(retryInterval);
                }
            }
            throw exception;
        }
    }
}
