﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace YotaCmd
{
    public class TrayIcon
    {
        NotifyIcon trayIcon = new NotifyIcon();

        public delegate void TrayIconClickEventHandler(object sender, EventArgs e);
        public event TrayIconClickEventHandler Click;

        public string Text
        {
            get
            {
                return trayIcon.Text;
            }
            set
            {
                trayIcon.Text = value;
            }
        }

        public TrayIcon(Icon icon)
        {
            trayIcon.Icon = (Icon)icon.Clone();
            trayIcon.Click += delegate(object sender, EventArgs e)
            {
                Click(this, e);
            };
        }

        public void Show()
        {
            trayIcon.Visible = true;
        }

        public void Hide()
        {
            trayIcon.Visible = false;
        }
    }
}
