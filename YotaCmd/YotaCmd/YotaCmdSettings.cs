﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Media;

namespace YotaCmd
{
    public class TimeTableDay
    {
        public List<TimetableTariffType> Tariff;
    }
    public class TimeTableDayColored
    {
        public List<TimetableTariffColored> Tariff { get; set; }
        public string Title { get; set; }
    }

    public enum TimetableTariffType
    {
        Inactive, Minimum, Maximum, User1, User2
    }
    public class TimetableTariffColors
    {
        public TimetableTariffType Type { get; set; }
        public Color Color { get; set; }
        
        public TimetableTariffColors(TimetableTariffType type, Color color)
        {
            Type = type;
            Color = color;
        }
    }
    public class TimetableTariffColored : INotifyPropertyChanged
    {
        public TimetableTariffType Type { get; set; }
        private string color;
        public string Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
                OnPropertyChanged("Color");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ApplicationSettings
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Autorun { get; set; }
        public bool ShowNotify { get; set; }
        public bool CheckForUpdates { get; set; }
        public bool EnableTimetable { get; set; }
        public bool MinimizeInTray { get; set; }
        public List<TimeTableDay> TimeTable { get; set; }
        public List<TimetableTariffColors> TariffColors { get; set; }

        public ApplicationSettings() { }

        public ApplicationSettings(object fail)
        {
            Autorun = false;
            ShowNotify = true;
            CheckForUpdates = false;
            MinimizeInTray = true;
            TimeTable = new List<TimeTableDay>();
            for (var i = 0; i < 7; i++)
            {
                var tariff = new List<TimetableTariffType>();
                for (var j = 0; j < 24; j++)
                {
                    tariff.Add(new TimetableTariffType());
                }
                TimeTable.Add(new TimeTableDay { Tariff = tariff });
            }
            TariffColors = new List<TimetableTariffColors>();
            TariffColors.Add(new TimetableTariffColors(TimetableTariffType.Inactive, new Color { R = 200, G = 200, B = 200, A = 255 }));
            TariffColors.Add(new TimetableTariffColors(TimetableTariffType.Minimum, new Color { R = 82, G = 174, B = 255, A = 255 }));
            TariffColors.Add(new TimetableTariffColors(TimetableTariffType.Maximum, new Color { R = 255, G = 106, B = 97, A = 255 }));
            TariffColors.Add(new TimetableTariffColors(TimetableTariffType.User1, new Color { R = 204, G = 255, B = 152, A = 255 }));
            TariffColors.Add(new TimetableTariffColors(TimetableTariffType.User2, new Color { R = 255, G = 157, B = 242, A = 255 }));
        }
    }

    public static class Settings
    {
        private static string AppName = "YotaCmd";
        private static string ConfigFile = "app.r1.config";

        public static ApplicationSettings Read()
        {
            ApplicationSettings result;
            var directory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\" + AppName;
            if (!(new DirectoryInfo(directory).Exists))
            {
                System.IO.Directory.CreateDirectory(directory);
            }

            using (var file = new StreamReader(new FileStream(directory + @"\" + ConfigFile, FileMode.OpenOrCreate)))
            {
                result = JsonConvert.DeserializeObject<ApplicationSettings>(file.ReadToEnd());
            }
            return result != null ? result : new ApplicationSettings(true);
        }

        public static void Write(ApplicationSettings settings)
        {
            var directory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\" + AppName;
            if (!(new DirectoryInfo(directory).Exists))
            {
                System.IO.Directory.CreateDirectory(directory);
            }

            using (var file = new StreamWriter(new FileStream(directory + @"\" + ConfigFile, FileMode.Create)))
            {
                file.Write(JsonConvert.SerializeObject(settings));
            }
        }
    }
}
