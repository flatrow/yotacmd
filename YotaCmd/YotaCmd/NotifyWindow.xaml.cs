﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using JavaScript.Timers;

namespace YotaCmd
{
    /// <summary>
    /// Interaction logic for NotifyWindow.xaml
    /// </summary>
    public partial class NotifyWindow : Window
    {
        public static void Show(string message)
        {
            NotifyWindow notifyWindow = new NotifyWindow(message);
            notifyWindow.Show();
        }
        public static void ShowFormat(string format, params string[] values)
        {
            NotifyWindow notifyWindow = new NotifyWindow(String.Format(format, values));
            notifyWindow.Show();
        }

        public NotifyWindow(string message)
        {
            InitializeComponent();
            textBlockMessage.Text = message;
            // Show window with animation
            Top = 30;
            Left = System.Windows.SystemParameters.PrimaryScreenWidth;
            DoubleAnimation moveAnimation = new DoubleAnimation(System.Windows.SystemParameters.PrimaryScreenWidth, System.Windows.SystemParameters.PrimaryScreenWidth - this.Width - 1, TimeSpan.FromSeconds(0.08));
            BeginAnimation(LeftProperty, moveAnimation);
            JavaScriptTimer.SetTimeout(() =>
                {
                    this.Dispatcher.Invoke(new Action(() =>
                        {
                            buttonClose_Click(null, null);
                        }), null);
                }, 4500);
        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            DoubleAnimation fadeAnimation = new DoubleAnimation(0.8, 0, TimeSpan.FromSeconds(0.25));
            fadeAnimation.Completed += fadeAnimation_Completed;
            BeginAnimation(OpacityProperty, fadeAnimation);
        }

        private void fadeAnimation_Completed(object sender, EventArgs e)
        {
            Close();
        }
    }
}
