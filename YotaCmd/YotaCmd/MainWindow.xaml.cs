﻿using Flatrow.Utils;
using JavaScript.Timers;
using NLog;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Linq;
using Yota.Classes;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace YotaCmd
{
    public partial class MainWindow : Window
    {
        protected Logger log = LogManager.GetCurrentClassLogger();

        protected IDisposable changeTariffTimer = JavaScriptTimer.SetTimeout(() => { }, 10);

        protected ApplicationSettings applicationSettings = Settings.Read();
        protected List<TimeTableDayColored> timetable;
        protected TimetableTariffType? currentType = null;

        protected YotaAccount account;
        protected YotaDevice device;
        protected YotaDeviceState deviceState;
        protected ApplicationCheckUpdate applicationCheckUpdate;

        protected SolidColorBrush activeItemColor, inactiveItemColor, selectedItemColor;

        protected TrayIcon trayIcon;

        public MainWindow()
        {
            InitializeComponent();
            InitializeConstants();

            ApplicationVersion.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Initialize values
        /// </summary>
        protected void InitializeConstants()
        {
            var color = new Color();
            // Active label
            color.R = 136;
            color.G = 136;
            color.B = 136;
            color.A = 255;
            activeItemColor = new SolidColorBrush(color);
            // Inactive label
            color.R = 187;
            color.G = 187;
            color.B = 187;
            color.A = 255;
            inactiveItemColor = new SolidColorBrush(color);
            // Selected label
            color.R = 0;
            color.G = 170;
            color.B = 238;
            color.A = 255;
            selectedItemColor = new SolidColorBrush(color);
        }

        /// <summary>
        /// Initialize connection on window loading
        /// </summary>
        private async void mainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            buttonConfigCancel_Click(null, null);
            stackPanelTimetable.DataContext = applicationSettings;

            timetable = applicationSettings.TimeTable.Select(hour =>
                new TimeTableDayColored
                {
                    Tariff = hour.Tariff.Join(applicationSettings.TariffColors, tariff => tariff, tariffColor => tariffColor.Type, (tariff, tariffColor) =>
                        new TimetableTariffColored { Type = tariffColor.Type, Color = tariffColor.Color.ToString() }).ToList()
                }).ToList();
            for (int i = 0; i < 7; i++)
            {
                timetable[i].Title = DayOfWeek(i);
            }
            dataGridTimatable.ItemsSource = timetable;

            trayIcon = new TrayIcon(Yota.Properties.Resources.Icon);
            trayIcon.Click += trayIcon_Clicked;
            trayIcon.Show();

            if (applicationSettings.Password == null)
            {
                stackPanel_MouseDown(stackPanelMenuItemConfig, null);
                return;
            }

            account = new YotaAccount(applicationSettings.Username, applicationSettings.Password);
            device = new YotaDevice(account);
            deviceState = new YotaDeviceState();
            applicationCheckUpdate = new ApplicationCheckUpdate();
            applicationCheckUpdate.UpdateAvailable += applicationCheckUpdate_UpdateAvailable;
            gridConnection.DataContext = deviceState;

            gridHelp.DataContext = this;

#if !DEBUG
            JavaScriptTimer.SetInterval(async () =>
                {
                    if (DateTime.Now.Minute == 0 && applicationSettings.EnableTimetable)
                    {
                        this.Dispatcher.Invoke(new Action(async () =>
                            {
                                await Connect();
                                int rusDayOfWeek = ((int)DateTime.Now.DayOfWeek > 0) ? (int)DateTime.Now.DayOfWeek - 1 : 6;
                                switch (applicationSettings.TimeTable[rusDayOfWeek].Tariff[DateTime.Now.Hour])
                                {
                                    case TimetableTariffType.Minimum:
                                        sliderTariff.Value = sliderTariff.Minimum;
                                        break;
                                    case TimetableTariffType.Maximum:
                                        sliderTariff.Value = sliderTariff.Maximum;
                                        break;
                                }
                            }), null);
                    }
                }, 60000);

            await Connect();
#else
            await device.account.ContinueFree();
#endif
        }

        private void applicationCheckUpdate_UpdateAvailable()
        {
            this.Dispatcher.Invoke(new Action(() => NotifyWindow.Show("Вышла новая версия приложения. Пожалуйста, посетите сайт yotacmd.ru чтобы загрузить обновленное приложение.")), null);
        }

        /// <summary>
        /// Mouse enters menu item
        /// </summary>
        private void stackPanelMenuItem_MouseEnter(object sender, MouseEventArgs e)
        {
            if (!CanChangeMenuItemColor(((StackPanel)sender)))
                return;
            // Set colors
            try
            {
                foreach (var child in ((StackPanel)sender).Children)
                {
                    if (child is Label)
                    {
                        ((Label)child).Foreground = selectedItemColor;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Changing color of menu item (mouse enter)", ex);
            }
        }

        /// <summary>
        /// Mouse leaves menu item
        /// </summary>
        private void stackPanelMenuItem_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!CanChangeMenuItemColor(((StackPanel)sender)))
                return;
            // Set colors
            try
            {
                foreach (var child in ((StackPanel)sender).Children)
                {
                    if (child is Label)
                    {
                        ((Label)child).Foreground = activeItemColor;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Changing color of menu item (mouse leave)", ex);
            }
        }

        /// <summary>
        /// Mouse clicks menu item
        /// </summary>
        private void stackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!CanChangeMenuItemColor(((StackPanel)sender)))
                return;
            stackPanelMenuItem_MouseEnter(sender, e);
            // Set colors
            foreach (var child in stackPanelMenu.Children)
            {
                if (child is StackPanel)
                {
                    try
                    {
                        if (((StackPanel)child).Tag.ToString().Equals("selected"))
                        {
                            ((StackPanel)child).Tag = "active";
                            stackPanelMenuItem_MouseLeave(child, null);
                            break;
                        }
                    }
                    catch (NullReferenceException)
                    {
                        log.Error("MenuItem Tag is not defined");
                        break;
                    }
                    catch (Exception ex)
                    {
                        log.ErrorException("Selecting menu item (mouse click)", ex);
                        break;
                    }
                }
            }
            ((StackPanel)sender).Tag = "selected";
            ShowGridByMenuItem(sender as StackPanel);
        }

        /// <summary>
        /// Can be color of menu item changed
        /// </summary>
        protected bool CanChangeMenuItemColor(StackPanel sender)
        {
            try
            {
                if (((StackPanel)sender).Tag.ToString().Equals("inactive") ||
                    ((StackPanel)sender).Tag.ToString().Equals("selected"))
                    return false;
            }
            catch (NullReferenceException)
            {
                log.Error("MenuItem Tag is not defined");
                return false;
            }
            catch (Exception ex)
            {
                log.ErrorException("Trying to find out whether the color of menu item can be changed", ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Show one grid and hide others
        /// </summary>
        /// <param name="sender">Grid to show</param>
        protected void ShowGridByMenuItem(StackPanel sender)
        {
            if (sender.Name.Equals("stackPanelMenuItemMain"))
                gridMain.Visibility = Visibility.Visible;
            else
                gridMain.Visibility = Visibility.Collapsed;
            if (sender.Name.Equals("stackPanelMenuItemConnection"))
                gridConnection.Visibility = Visibility.Visible;
            else
                gridConnection.Visibility = Visibility.Collapsed;
            if (sender.Name.Equals("stackPanelMenuItemTimetable"))
                gridTimetable.Visibility = Visibility.Visible;
            else
                gridTimetable.Visibility = Visibility.Collapsed;
            if (sender.Name.Equals("stackPanelMenuItemConfig"))
                gridConfig.Visibility = Visibility.Visible;
            else
                gridConfig.Visibility = Visibility.Collapsed;
            if (sender.Name.Equals("stackPanelMenuItemHelp"))
                gridHelp.Visibility = Visibility.Visible;
            else
                gridHelp.Visibility = Visibility.Collapsed;
        }

        private string DayOfWeek(int day)
        {
            switch (day)
            {
                case 0: return "ПН";
                case 1: return "ВТ";
                case 2: return "СР";
                case 3: return "ЧТ";
                case 4: return "ПТ";
                case 5: return "СБ";
                case 6: return "ВС";
                default: return "NaN";
            }
        }

        private async Task Connect()
        {
            try
            {
                await account.Connect();
                await device.Connect();
                sliderTariff.Maximum = device.TariffList.Count - 1;
                sliderTariff.Value = device.TariffList.Index;
                stackPanelTariff.DataContext = device.TariffList[device.TariffList.Index];
                stackPanelTariff.IsEnabled = true;
            }
            catch (UnauthorizedAccessException)
            {
                log.Info("Failed to log in account: wrong username or password");
                MessageBox.Show("Невозможно войти в аккаунт: неверные логин или пароль");
            }
            catch (Exception ex)
            {
                log.ErrorException("Logging in account failed", ex);
                MessageBox.Show("Не удалось войти в аккаунт");
            }
        }

        /// <summary>
        /// Update information when slider's value is changed
        /// </summary>
        private void sliderTariff_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                stackPanelTariff.DataContext = device.TariffList[(byte)sliderTariff.Value];
                if ((byte)sliderTariff.Value != (byte)e.OldValue && (byte)sliderTariff.Value != device.TariffList.Index)
                {
                    changeTariffTimer.Dispose();
                    changeTariffTimer = JavaScriptTimer.SetTimeout(() => this.Dispatcher.Invoke(new Action(async () =>
                    {
                        try
                        {
                            await device.SetTariff((byte)sliderTariff.Value);
                            sliderTariff.Value = device.TariffList.Index;
                            stackPanelTariff.DataContext = device.TariffList[(byte)sliderTariff.Value];
                            if (applicationSettings.ShowNotify)
                            {
                                NotifyWindow.ShowFormat("Установлен тариф со скоростью {0} {1} за {2} {3}",
                                    device.TariffList[device.TariffList.Index].Speed, device.TariffList[device.TariffList.Index].InfoSpeed,
                                    device.TariffList[device.TariffList.Index].Price, device.TariffList[device.TariffList.Index].InfoPrice);
                            }
                        }
                        catch (Exception ex)
                        {
                            log.ErrorException("Trying to set new tariff after slider's value changing", ex);
                        }
                    }), null), 2000);
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Trying to update information after slider's value changing", ex);
            }
        }

        private async void buttonConfigSave_Click(object sender, RoutedEventArgs e)
        {
            if (!textBoxPassword.Password.Equals("12345678901234567890"))
            {
                applicationSettings.Password = textBoxPassword.Password;
                account = new YotaAccount(applicationSettings.Username, applicationSettings.Password);
                device = new YotaDevice(account);
                await Connect();
            }
            Settings.Write(applicationSettings);
            NotifyWindow.Show("Изменения в настройках программы успешно сохранены");
        }

        private void buttonConfigCancel_Click(object sender, RoutedEventArgs e)
        {
            applicationSettings = Settings.Read();

            stackPanelConfig.DataContext = applicationSettings;
            if (applicationSettings.Password == null)
            {
                textBoxPassword.Password = "";
            }
        }

        private void textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void passwordBoxPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            ((PasswordBox)sender).SelectAll();
        }

        private void dataGridTimatable_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dataGridTimatable.CurrentItem == null)
            {
                return;
            }
            int index = dataGridTimatable.CurrentColumn.DisplayIndex;
            var item = (TimeTableDayColored)dataGridTimatable.CurrentItem;
            if (currentType == null)
            {
                currentType = NextType(item.Tariff[index].Type);
            }
            item.Tariff[index].Type = (TimetableTariffType)currentType;
            item.Tariff[index].Color = applicationSettings.TariffColors.First(option => option.Type == currentType).Color.ToString();
        }

        private TimetableTariffType NextType(TimetableTariffType type)
        {
            switch (type)
            {
                case TimetableTariffType.Minimum: return TimetableTariffType.Maximum;
                case TimetableTariffType.Maximum: return TimetableTariffType.Inactive;
                case TimetableTariffType.Inactive: return TimetableTariffType.Minimum;
            }
            return TimetableTariffType.Minimum;
        }

        private void dataGridTimatable_MouseUp(object sender, MouseButtonEventArgs e)
        {
            currentType = null;
            dataGridTimatable.ItemsSource = null;
            for (int i = 0; i < 7; i++)
            {
                for (int j = 0; j < 24; j++)
                {
                    applicationSettings.TimeTable[i].Tariff[j] = timetable[i].Tariff[j].Type;
                }
            }
            Settings.Write(applicationSettings);
            timetable = applicationSettings.TimeTable.Select(hour =>
                new TimeTableDayColored
                {
                    Tariff = hour.Tariff.Join(applicationSettings.TariffColors, tariff => tariff, tariffColor => tariffColor.Type, (tariff, tariffColor) =>
                        new TimetableTariffColored { Type = tariffColor.Type, Color = tariffColor.Color.ToString() }).ToList()
                }).ToList();

            for (int i = 0; i < 7; i++)
            {
                timetable[i].Title = DayOfWeek(i);
            }

            dataGridTimatable.ItemsSource = timetable;
        }

        private void CheckBox_IsCheckedChanged(object sender, RoutedEventArgs e)
        {
            Settings.Write(applicationSettings);
        }

        private void trayIcon_Clicked(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
            {
                WindowState = WindowState.Normal;
                ShowInTaskbar = true;
                Activate();
            }
            else
            {
                WindowState = WindowState.Minimized;
                if (applicationSettings.MinimizeInTray)
                {
                    ShowInTaskbar = false;
                }
            }
        }

        private void mainWindow_StateChanged(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Minimized && applicationSettings.MinimizeInTray)
            {
                ShowInTaskbar = false;
            }
        }

        private void mainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            trayIcon.Hide();
        }
    }
}