﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Flatrow.Utils
{
    public enum HttpWebRequestMethod { Get, Post }

    public static class HttpWebRequestExtension
    {
        public static void MaskBrowser(this HttpWebRequest obj)
        {
            obj.MediaType = "HTTP/1.1";
            obj.Headers.Add("Accept-Language", "ru");
            obj.Headers.Add("Accept-Charset", "utf-8");
            obj.Accept = "text/html";
            obj.ContentType = "application/x-www-form-urlencoded";
            obj.KeepAlive = true;
            obj.AllowAutoRedirect = false;
        }

        public static void MaskChrome(this HttpWebRequest obj)
        {
            obj.MaskBrowser();
            obj.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.154 Safari/537.36 OPR/20.0.1387.91";
        }

        public static void SetRequestData(this HttpWebRequest obj, string data)
        {
            var byteData = Encoding.UTF8.GetBytes(data);
            obj.ContentLength = byteData.Length;
            var stream = obj.GetRequestStream();
            stream.Write(byteData, 0, byteData.Length);
            stream.Close();
        }

        public static string GetResponseString(this HttpWebResponse obj)
        {
            var stream = obj.GetResponseStream();
            if (stream == null)
                return "";
            var reader = new StreamReader(stream);
            var result = reader.ReadToEnd();
            return result;
        }

        public static double FromBytesToValue(this double obj)
        {
            const int del = 1024;
            return obj < del ? obj : ((double)Math.Round(obj / del, 2)).FromBytesToValue();
        }

        public static long FromBytesToValue(this long obj)
        {
            const int del = 1024;
            return (long)(obj < del ? obj : ((double)Math.Round((double)obj / del, 2)).FromBytesToValue());
        }

        public static string FromBytesToTitle(this long obj)
        {
            const int del = 1024;
            if (obj < del)
                return "";
            if (obj / (del) < del)
                return "к";
            if (obj / (del * del) < del)
                return "м";
            return obj / (del * del * del) < del ? "г" : "т";
        }

        public static string FromBytesToTitle(this double obj)
        {
            return ((long)obj).FromBytesToTitle();
        }

        public static async Task<HttpWebResponse> PureGetResponseAsync(this HttpWebRequest request)
        {
            return (HttpWebResponse)await request.GetResponseAsync();
        }
    }
}