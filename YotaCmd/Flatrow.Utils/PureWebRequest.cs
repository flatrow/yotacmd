﻿using System;
using System.Net;

namespace Flatrow.Utils
{
    public class PureWebRequest
    {
        private HttpWebRequest request;

        public PureWebRequest(Uri requestUri, HttpWebRequestMethod requestMethod, CookieContainer requestCookies = null, string requestReferer = null)
        {
            request = (HttpWebRequest)WebRequest.Create(requestUri);
            request.Timeout = 8000;
            request.MaskChrome();
            request.Method = requestMethod.ToString();
            if (requestCookies != null)
                request.CookieContainer = requestCookies;
            if (requestReferer != null)
                request.Referer = requestReferer;
        }
        public PureWebRequest(string requestUri, HttpWebRequestMethod requestMethod, CookieContainer requestCookies = null, string requestReferer = null)
        {
            request = (HttpWebRequest)WebRequest.Create(requestUri);
            request.Timeout = 8000;
            request.MaskChrome();
            request.Method = requestMethod.ToString();
            if (requestCookies != null)
                request.CookieContainer = requestCookies;
            if (requestReferer != null)
                request.Referer = requestReferer;
        }
        public static implicit operator HttpWebRequest(PureWebRequest pureWebRequest)
        {
            return pureWebRequest.request;
        }
    }
}
