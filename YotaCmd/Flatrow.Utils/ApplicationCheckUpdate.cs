﻿using System;

namespace Flatrow.Utils
{
    public class ApplicationCheckUpdate : ApplicationRequestor
    {
        public delegate void UpdateAvailableHandler();

        public event UpdateAvailableHandler UpdateAvailable;

        protected const string UpdateRequestFormat = "CheckUpdate?version={0}";

        public ApplicationCheckUpdate()
        {
            RequestMethod = String.Format(UpdateRequestFormat, System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);
            ProcessResponse += ApplicationCheckUpdate_ProcessResponse;
        }

        protected void ApplicationCheckUpdate_ProcessResponse(BaseResponse<dynamic> response)
        {
            if (response.Success)
            {
                switch (response.Version)
                {
                    case 1:
                        var result = response.Data as string;
                        if (result != null && result.Equals("YES"))
                        {
                            if (UpdateAvailable != null)
                            {
                                UpdateAvailable();
                            }
                        }
                        break;
                }
            }
        }
    }
}
