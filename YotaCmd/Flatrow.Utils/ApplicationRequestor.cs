﻿using JavaScript.Timers;
using Newtonsoft.Json;
using NLog;
using System;
using System.Net;
using System.Timers;

namespace Flatrow.Utils
{
    public enum RequestorType
    {
        Periodic, OneTime
    }

    public abstract class ApplicationRequestor
    {
        protected Logger log = LogManager.GetCurrentClassLogger();

        protected delegate void ProcessResponseHandler(BaseResponse<dynamic> response);
        protected event ProcessResponseHandler ProcessResponse;

        private const int RequestTimeout = 5 * 1000;
        private const int RequestInterval = 60 * 60 * 1000;
#if DEBUG
        private const string BaseUrl = "http://yotacmd.local/api/";
#else
        private const string BaseUrl = "http://yotacmd.ru/api/";
#endif

        public string RequestMethod { get; set; }

        public string RequestParameters { get; set; }

        public string RequestUrl
        {
            get { return BaseUrl + RequestMethod; }
        }

        protected ApplicationRequestor(RequestorType requestorType = RequestorType.Periodic)
        {
            switch (requestorType)
            {
                case RequestorType.OneTime:
                    break;

                case RequestorType.Periodic:
                    var updateTimer = new Timer(RequestInterval);
                    updateTimer.Elapsed += (sender, args) => Request();
                    updateTimer.Start();
                    break;
            }
            JavaScriptTimer.SetTimeout(Request, RequestTimeout);
        }

        public async void Request()
        {
            try
            {
                await Operation.ExecuteWithRetryAsync(async () =>
                {
                    HttpWebRequest request = new PureWebRequest(RequestUrl, HttpWebRequestMethod.Get);
                    if (!String.IsNullOrEmpty(RequestParameters))
                    {
                        request.SetRequestData(RequestParameters);
                    }
                    using (var response = await request.PureGetResponseAsync())
                    {
                        if (ProcessResponse != null)
                        {
                            ProcessResponse(JsonConvert.DeserializeObject<BaseResponse<dynamic>>(response.GetResponseString()));
                        }
                    }
                }, new TimeSpan(0, 0, 5));
            }
            catch (Exception)
            {
                log.Error("Unable to complete request to {0} with parameters {1}", RequestUrl, RequestParameters);
            }
        }
    }
}