﻿using System.Collections.Generic;
using System.Net;

namespace Flatrow.Utils
{
    public class BaseResponse
    {
        public int Status { get; set; }

        public int Version { get; set; }

        public bool Success { get; set; }

        public BaseResponse(HttpStatusCode status = HttpStatusCode.OK)
        {
            Success = true;
            Status = (int)status;
        }
    }

    public class BaseResponse<T> : BaseResponse
    {
        public T Data { get; set; }

        public BaseResponse(T data, HttpStatusCode status = HttpStatusCode.OK)
            : base(status)
        {
            Data = data;
        }
    }

    public class ErrorResponse : BaseResponse<IEnumerable<string>>
    {
        public ErrorResponse(HttpStatusCode status, params string[] errors)
            : base(errors, status)
        {
            Success = false;
        }

        public ErrorResponse(params string[] errors)
            : this(HttpStatusCode.OK, errors)
        {
        }
    }
}